<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entities\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => (string)$faker->randomDigit(1,1000),
        'user_id' => function(){
            return factory(\App\User::class)->create()->id;
        },
    ];
});
