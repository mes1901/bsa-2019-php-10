<?php

namespace Tests\Feature;

use App\Entities\Product;
use App\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MarketApiTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testShowProductList()
    {
        factory(User::class, 3)
            ->create()
            ->each(function ($user) {
                $user->products()->saveMany(
                    factory(Product::class, 3)
                        ->make(['user_id' => null])
                );
            });
        $response = $this->json('GET', '/api/items');
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'name',
                'price',
                'user_id',
            ]
        ]);
    }

    public function testShowProductListFailed()
    {
        $response = $this->json('GET', '/api/items');
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertStatus(400);
        $response->assertJson([
            "message" => "there is no products",
        ]);
    }

    public function testStoreProductAsNonAuth()
    {
        $response = $this->json('POST', '/api/items');
        $response
            ->assertStatus(403)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJson([
                    "message" => "Forbidden",
                ]);
    }

    public function testStoreProductAsAuth()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->make();
        Auth::loginUsingId($user->id);
        $response = $this->actingAs($user)->json('POST', '/api/items', [
            'product_name' => $product->name,
            'product_price' => $product->price
        ]);


        $response->assertHeader('Content-Type', 'application/json');
        $response->assertStatus(201);
        $response->assertJsonStructure([
            'id',
            'name',
            'price',
            'user_id',
        ]);
    }

    public function testShowProduct()
    {
        $product = factory(Product::class)->create();
        $response = $this->json('GET', '/api/items/' . $product->id, [
            'id' => $product->id,
        ]);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id',
            'name',
            'price',
            'user_id',
        ]);
    }

    public function testShowProductFailed()
    {
        $product = factory(Product::class)->create();
        $response = $this->json('GET', '/api/items/' . ($product->id + 1), [
            'id' => $product->id + 1,
        ]);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertStatus(400);
        $response->assertJson([
                "message" => "failed to show product",
            ]);
    }

    public function testDeleteProductAsNonAuth()
    {
        $user = factory(User::class)->create(['id' => 1]);
        $product = factory(Product::class)->create([
            'user_id' => $user->id
        ]);
        $response = $this->json('DELETE', '/api/items/' . $product->id, [
            'id' => $product->id,
        ]);
        $response
            ->assertStatus(403)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJson([
                    "message" => "Forbidden",
                ]);
    }

    public function testDeleteProductAsAuth()
    {
        $product = factory(Product::class)->create();
        $user = Auth::loginUsingId($product->user_id);
        $response = $this->actingAs($user)->json('DELETE', '/api/items/' . $product->id, [
            'id' => $product->id,
        ]);
        $response->assertStatus(204);
    }

    public function testDeleteProductAsNonOwner()
    {
        $user = factory(User::class)->create();
        Auth::loginUsingId($user->id);
        $product = factory(Product::class)->create([
            'user_id' => $user->id
        ]);
        $response = $this->actingAs($user)->json('DELETE', '/api/items/' . ($product->id + 1), [
            'id' => $product->id + 1,
        ]);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertStatus(400);
        $response->assertJson([
            "message" => "you can not delete the product",
        ]);
    }
}
