<?php

namespace Tests\Unit;

use App\Entities\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Services\MarketService;
use Tests\CreatesApplication;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class MarketServiceTest extends TestCase
{
    use CreatesApplication;
    use WithFaker;

    private $productRepository;
    private $marketService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->productRepository = $this->createMock(ProductRepositoryInterface::class);
        $this->marketService = new MarketService($this->productRepository);
    }

    /** @test */
    public function TestGetProductById()
    {
        $product = factory(Product::class)->make(['id' => 1]);
        $this->productRepository->method('findById')->willReturn($product);
        $productResponse = $this->marketService->getProductById(1);
        $this->assertEquals($productResponse->id, $product->id);
        $this->assertEquals($productResponse->name, $product->name);
        $this->assertEquals($productResponse->price, $product->price);
        $this->assertEquals($productResponse->user_id, $product->user_id);
    }

    /** @test */
    public function TestGetProductsByUserId()
    {
        $products = factory(Product::class, 3)->make(['user_id' => 1]);
        $this->productRepository->method('findByUserId')->willReturn($products);
        $productList = $this->marketService->getProductsByUserId(1);
        foreach ($productList as $product) {
            $this->assertInstanceOf(Product::class, $product);
        }

    }

    /** @test */
    public function TestGetProductList()
    {
        $products = factory(Product::class, 3)->make();
        $this->productRepository->method('findAll')
            ->willReturn($products);

        $productList = $this->marketService->getProductList();

        foreach ($productList as $product) {
            $this->assertInstanceOf(Product::class, $product);
        }
    }

    /** @test */
    public function TestStoreProduct()
    {
        $product = factory(Product::class)->make();

        $this->productRepository->method('store')
            ->willReturn($product);
        $this->assertInstanceOf(Product::class, $product);
    }

}
