<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use App\Services\MarketService;
use Illuminate\Support\Facades\Auth;

class MarketApiController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showList() {
        $products = $this->marketService->getProductList();
        if (count($products) < 1) {
            return response()->json([
                'message' => 'there is no products'],
                400
            );
        }
        $productsResponse = [];
        foreach ($products as $product){
            $productsResponse[] = new ProductResource($product);
        }
        return response()->json($productsResponse, 200);
    }

    public function store(Request $request) {
        if(!Auth::user()) {
            return response()->json(['message' => 'Forbidden'], 403);
        }
        try {
            $product = $this->marketService->storeProduct($request);
        } catch (\Exception $e) {
            return response()->json(['message' => 'failed to added product'], 400);
        }
        return response()->json(new ProductResource($product), 201);
    }

    public function showProduct(int $id) {
        try {
            $product = $this->marketService->getProductById($id);
        }catch (\Exception $e) {
            return response()->json(['message' => 'failed to show product'], 400);
        }
        return response()->json(new ProductResource($product), 200);
    }

    public function delete(Request $request) {
        if(!Auth::user()) {
            return response()->json(['message' => 'Forbidden'], 403);
        }
        try {
            $product = $this->marketService->getProductById($request->id);
            if(Auth::id() == $product->user_id){
                $this->marketService->deleteProduct($request);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'you can not delete the product'], 400);
        }
        return response()->json()->setStatusCode(204);
    }
}
