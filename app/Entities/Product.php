<?php

namespace App\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'id',
        'name',
        'price',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
